// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API ATestPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditAnywhere, Category = "Settings")
	TSubclassOf<UUserWidget> MenuWidgetClass;

	UPROPERTY(EditAnywhere, Category = "Settings")
	TArray<float> Speeds;
	
	virtual void BeginPlay() override;
	
	virtual void SetupInputComponent() override;
	
	template<int Value>
	void ChangeSpeed_Helper()
	{
		ChangeSpeedPawn(Value);
	}
	
	void ChangeSpeedPawn(float value);
};
