// Fill out your copyright notice in the Description page of Project Settings.


#include "SphereSpline.h"

// Sets default values
ASphereSpline::ASphereSpline()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MyRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(MyRootComponent);

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	SplineComponent->SetupAttachment(GetRootComponent());

	Sphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sphere"));
	Sphere->SetupAttachment(GetRootComponent());
}

// Called every frame
void ASphereSpline::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Distance = (DeltaTime * Speed) + Distance;

	if(Sphere && SplineComponent)
	{
		FTransform SplineTransform = SplineComponent->GetTransformAtDistanceAlongSpline(Distance, ESplineCoordinateSpace::Local);

		FTransform NewTransform = FTransform(SplineTransform.GetRotation(), SplineTransform.GetLocation(), FVector(1.f) );
		Sphere->SetRelativeTransform(NewTransform);

		if(Distance > SplineComponent->GetSplineLength())
		{
			Distance = 0.f;
		}
	}
}

